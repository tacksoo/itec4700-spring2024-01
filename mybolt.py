import time
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-C81A')

def travel_circle(droid):
    for i in range(0,360,10):
        droid.roll(i, 10, 0.5)

def travel_straight_line(droid):
    # this is using different heading
    droid.roll(0, 35, 3)
    time.sleep(2)
    droid.roll(180, 35, 3)
    time.sleep(2)

def travel_in_triangle(droid):
    droid.roll(0, 35, 1)
    time.sleep(1)
    droid.roll(150, 35, 1)
    time.sleep(1)
    droid.roll(270, 35, 1)
    time.sleep(1)

def do_something_interesting(droid):
    s = input("Write something interesting: ")
    droid.scroll_matrix_text(s, Color(randint(0,255), randint(0,255),randint(0,255)), 5, False)
    if len(s) > 10:
        droid.roll(0, 100, 1)
    else:
        droid.roll(-180, 100, 1)

with SpheroEduAPI(toy) as droid:
    while True:
        droid.set_front_led(Color(0,255,0))
        droid.set_back_led(Color(255,0,0))
        #droid.scroll_matrix_text("i love robots!", Color(0,0,255),7,False)
        do_something_interesting(droid)