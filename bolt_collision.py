
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI, EventType
import time
from spherov2.types import Color
from random import randint

#def on_collision(droid: SpheroEduAPI):
#    print("collision!")

def on_ir_message(droid: SpheroEduAPI):
    print("ir message!")

def on_freefall(droid: SpheroEduAPI):
    print("freefall!")

def flip_direction(heading):
    return (heading + 180) % 360

def on_collision(droid: SpheroEduAPI):
    #droid.stop_roll()
    #droid.set_main_led(Color(255, 0, 0))
    print('Collision')
    heading = droid.get_heading()
    print(heading)
    heading = flip_direction(heading)
    print(heading)
    droid.roll(heading, 200, 10)
    time.sleep(1)
    #droid.set_heading(droid.get_heading() + 180)
    #time.sleep(0.5)
    #droid.set_main_led(Color(255, 22, 255))
    #droid.set_speed(100)



toy = scanner.find_toy(toy_name='SB-C81A')
with SpheroEduAPI(toy) as droid:
  droid.send_ir_message()
  droid.register_event(EventType.on_collision, on_collision)
  #droid.register_event(EventType.on_ir_message, on_ir_message)
  #droid.register_event(EventType.on_freefall, on_freefall)
  droid.roll(0, 200, 5)
  while True:
      pass



#droid.register_event(EventType.on_sensor_streaming_data, droid.SensorStreamingInfo)
