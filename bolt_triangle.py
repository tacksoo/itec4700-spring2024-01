from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import time
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-C81A')
print(toy.name)

with SpheroEduAPI(toy) as droid:
    while True:
        droid.roll(0, 35, 1)
        time.sleep(1)
        droid.roll(150, 35, 1)
        time.sleep(1)
        droid.roll(270, 35,1)
        time.sleep(1)

