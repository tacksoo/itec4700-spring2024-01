from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import time
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-0323')
print(toy.name)
with SpheroEduAPI(toy) as droid:
    droid.start_ir_broadcast(0, 1)
    while True:
        r = randint(0,255)
        g = randint(0,255)
        b = randint(0,255)
        droid.set_front_led(Color(r,g,b))
        #droid.send_ir_message(0, 32)
        time.sleep(5)
        pass
        #droid.send_ir_message(1,32)
