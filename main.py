i = 10
name = "dr.im"
is_rainy = True  # False
temp = 42.5

# builtin python function, about 50 of them
x = pow(5, 2)
print(x)

mood = input("How do you feel about AI? ")

if mood == "excited":
    print("me too!")
    print("i am super excited about AI")
else:
    print("that's cool too")
    print("it's okay to feel " + mood)

num = input("How many times do you want to repeat: ")
num = int(num)  # int(), float(), str()
i = 0
while i <= num:
    print("AI is the future")
    i += 1

scores = [100, 90, 80, 77, 99, 60, 30, 100]
print(sum(scores)/len(scores))

print(scores[0])
print(scores[1])
print(scores[-1])




