import heapq

class Entry:
    def __init__(self, distance=None, letter=None):
        self.distance = distance
        self.letter = letter

    def __lt__(self, other):
        return self.distance < other.distance

    def __eq__(self, other):
        return self.distance == other.distance and self.letter == other.letter


def get_min_cost_from(letter, graph):
    letter_map = {}
    cost = []

    # Initialize priority queue and letterMap with entries
    for c in graph:
        entry = Entry()
        entry.letter = c
        if c == letter:
            entry.distance = 0
        else:
            entry.distance = float('inf')
        heapq.heappush(cost, entry)
        letter_map[c] = entry

    while cost:
        entry = heapq.heappop(cost)
        connections = graph[entry.letter]
        for target in connections:
            cost_this_way = entry.distance + connections[target]
            so_far = letter_map[target]
            if cost_this_way < so_far.distance:
                letter_map[target].distance = cost_this_way
                heapq.heappush(cost, letter_map[target])

    result = {}
    for c in letter_map:
        result[c] = letter_map[c].distance

    return result


graph = { "a": {"b":1, "c":2}, "b" : {"a":1, "c":0.5} , "c": {"a": 2, "b":0.5}}

print(get_min_cost_from("a",graph))
