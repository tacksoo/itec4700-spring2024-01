
class Game:
    def __init__(self, id, name, price):
        self.id = id
        self.name = name
        self.price = price

    def __lt__(self, other):
        return self.id < other.id

    def __eq__(self, other):
        return self.id == other.id

    def __repr__(self):
        return f'{self.name}'

    def __str__(self):
        return f'{self.name} has id {self.id} and costs ${self.price}'

zelda = Game(5, "Legend of Zelda", 100)
tetris = Game(3, "Tetris", 30)
mario = Game(1,"Super Mario Bros", 60.50)

games = [tetris, mario, zelda]

print(sorted(games))
print(mario)


