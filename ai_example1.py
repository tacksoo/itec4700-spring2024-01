# from replit.com ai
# give me an algorithm to detect the first unique character in a string

def first_unique_char(s):
    char_count = {}
    # create a dictionary to store the count of each character in the string
    for char in s:
        if char in char_count:
            char_count[char] += 1
        else:
            char_count[char] = 1
    # iterate through the string and return the first character with count 1
    for i in range(len(s)):
        if char_count[s[i]] == 1:
            return i
    return -1

# Test cases
print(first_unique_char("leetcode"))  # Output: 0
print(first_unique_char("loveleetcode"))  # Output: 2
print(first_unique_char("abcabc"))  # Output: -1