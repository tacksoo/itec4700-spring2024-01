
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI, EventType
import time
from spherov2.types import Color
from random import randint

i = 0
def on_collision(droid: SpheroEduAPI):
    global i;
    i += 1
    print(f'Collision detected {i}')
    r = randint(0, 255)
    g = randint(0, 255)
    b = randint(0, 255)
    droid.set_front_led(Color(r, g, b))
    new_heading = (droid.get_heading() + 180) % 360
    #droid.spin(new_heading, 1)
    #droid.set_speed(200)
    droid.roll(new_heading, 200, 3)


toy = scanner.find_toy(toy_name='SB-847C')
with SpheroEduAPI(toy) as droid:
  droid.register_event(EventType.on_collision, on_collision)
  #droid.roll(0, 200, 5)
  while True:
      pass