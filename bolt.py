from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import time
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-0323')
print(toy.name)
#print(toy.name) - this gets a random name if you don't specify the name
with SpheroEduAPI(toy) as droid:
  while True:
    droid.scroll_matrix_text("i love you", Color(250, 0, 0), 15, False)
    droid.set_front_led(Color(255, 0, 0))
    #droid.set_matrix_character("v", Color(255,0,0))
    red = randint(0,255)
    green = randint(0,255)
    blue = randint(0,255)
      #droid.set_main_led(Color(r=red, g=green, b=blue))
    droid.spin(180, 2)
    droid.roll(0,100,3)
    time.sleep(3)
    droid.roll(180,100, 3 )
      #droid.set_main_led()
    time.sleep(3)
    '''
    roid.roll(0, 70, 1)
    droid.play_sound(1)
    time.sleep(2)
    droid.roll(90,70, 1)
    time.sleep(2)
    droid.roll(90,70, 1)
    time.sleep(2)
    '''