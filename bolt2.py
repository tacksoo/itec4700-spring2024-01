import time
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
from spherov2.types import Color

toy = scanner.find_toy(toy_name='SB-6C31')
with SpheroEduAPI(toy) as droid:
    droid.set_speed(60)
    droid.roll(0,30,10)
    time.sleep(2)
    droid.set_speed(0)
