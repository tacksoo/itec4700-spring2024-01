class BinaryNode:
    def __init__(self, value, left_child=None, right_child=None):
        self.value = value
        self.left_child = left_child
        self.right_child = right_child

    def __str__(self):
        return f"BinaryNode: value={self.value}"

seventytwo = BinaryNode(72)
fortyone = BinaryNode(41)
thirtythree = BinaryNode(33)
twentyone = BinaryNode(21)
sixty = BinaryNode(60,right_child=seventytwo)
fifty = BinaryNode(50,right_child=sixty)
thirtyeight = BinaryNode(38,thirtythree,fortyone)
twentyseven = BinaryNode(27,twentyone,thirtyeight)
root = BinaryNode(42,twentyseven,fifty)

from collections import deque

d = deque()
d.append(root)

while len(d) > 0:
    front = d.popleft()
    print(front.value, end=" ")
    if front.left_child is not None:
        d.append(front.left_child)
    if front.right_child is not None:
        d.append(front.right_child)



