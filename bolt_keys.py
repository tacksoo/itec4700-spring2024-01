import math
import os
import sys
import threading
from datetime import datetime
from typing import List, Tuple, Iterator, Dict, Any

from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
from spherov2.types import Color


if sys.platform.startswith('win32'):
    arrow_key_mapping = {
        72: 'up',
        80: 'down',
        77: 'right',
        75: 'left'
    }
    regular_key_mapping = {
        27: 'esc',
        9: 'tab',
        32: 'space',
        13: 'return',
        8: 'backspace'
    }

    import msvcrt


    def get_key() -> str:
        k = ord(msvcrt.getch())
        if k != 0x00 and k != 0xe0:
            return regular_key_mapping.get(k, chr(k))
        k = ord(msvcrt.getch())
        return arrow_key_mapping.get(k, None)

else:
    arrow_key_mapping = {
        65: 'up',
        66: 'down',
        67: 'right',
        68: 'left'
    }
    regular_key_mapping = {
        27: 'esc',
        9: 'tab',
        32: 'space',
        10: 'return',
        127: 'backspace'
    }

    import tty
    import termios


    def get_key() -> str:
        old_settings = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin.fileno())
        try:
            while True:
                b = os.read(sys.stdin.fileno(), 3).decode()
                if len(b) == 3:
                    k = ord(b[2])
                    return arrow_key_mapping.get(k, None)
                else:
                    k = ord(b)
                    return regular_key_mapping.get(k, chr(k))
        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)


from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI, EventType
import time
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-847C')
with SpheroEduAPI(toy) as droid:
  speed_delta = 5
  angle_delta = 5
  while True:
      speed = droid.get_speed()
      heading = droid.get_heading()
      print(f'Current speed is: {speed}')
      print(f'Current heading is: {heading}')
      x = get_key()
      if x == 'up':
          droid.set_speed(speed + speed_delta)
      elif x == 'down':
          droid.set_speed(speed - speed_delta)
      elif x == 'left':
          new_heading = (heading - angle_delta) % 360
          droid.set_heading(new_heading)
      elif x == 'right':
          new_heading = (heading + angle_delta) % 360
          droid.set_heading(new_heading)
