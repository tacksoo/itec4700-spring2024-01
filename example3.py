# open the file
file = open("numbers.csv")

# read the file
text = file.read()
print(text)

# split it into lines
lines = text.split("\n")

for line in lines:
    nums = line.split(",")
    for num in nums:
        print(num[0])

# research: dictionary in python
# use it to keep track of count of 1's, 2's, 3's etc.
# more research: Counter
