
temp = 49.5
is_cold = True
nums = [42]
word = "bird " * 10
print(word)
# truthy and falsey

if temp >= 50:
    print("it's cold!")

if temp >= 50:
    print("it's cold")
else:
    print("it's okay")

if temp >= 90:
    print("it's hot")
elif temp >= 60:
    print("it's just right")
elif temp >= 50:
    print("it's cool")
else:
    print("oh no! it's too cold")

i = 0
while i < 10:
    print("i'm in a loop")
    i += 1

while True:
    word = input("What's the word? ")
    if word.lower() == "bird":
        break
    else:
        print("not the word!")

#import student
from student import Student
t = Student("student t", 90008765, 4.0)
print(t)