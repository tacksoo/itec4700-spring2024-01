
import heapq

nums = []

heapq.heappush(nums, 10)
heapq.heappush(nums, 42)
heapq.heappush(nums, 2)
heapq.heappush(nums, 4)
heapq.heappush(nums, 12)
heapq.heappush(nums, 100)

print(heapq.nsmallest(3, nums))
print(heapq.nlargest(3, nums))

'''
print(heapq.heappop(nums))
print(heapq.heappop(nums))
print(heapq.heappop(nums))
print(heapq.heappop(nums))
'''



