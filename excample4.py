

#dictionary vs list

names = ["Adis", "Shantel", "Lucas", "Harmony", "Kelechi"]

print(names[0])
print(names[1])
print(names[-1])
print(names[-2])
print(names[1:4])
print(names[0:2])
singers = ["Taylor", "Drake", "BTS"]
actors = ["Tom", "Justin", "John"]
celebrities = singers + actors
print(celebrities)
singers = singers + ["Selena"]
singers.append("Mozart")
print(singers)

# dictionaries use key, value pairs
courses = { 4700:"artificial intelligence", 3860:"software dev 1",
            4260:"software testing and qa"}
print(courses[4700])
courses[1001] = "intro to computing"
print(courses)

print(2140 in courses)
print(4700 in courses)





