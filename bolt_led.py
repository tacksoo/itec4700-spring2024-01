from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import time
from spherov2.types import Color
from random import randint
from random import choice

toy = scanner.find_toy(toy_name='SB-0323')
print(toy.name)

colors = { 'green': Color(0,255,0), 'red': Color(255,0,0), 'blue': Color(0,0, 255),
           'white' : Color(255,255,255), 'brown': Color(255,255,0)

           }

with SpheroEduAPI(toy) as droid:
    while True:
       droid.set_front_led( colors[choice(['green', 'red','blue'])] )
       time.sleep(2)
