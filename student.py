class Student:
    def __init__(self, name, id, gpa=0.0):
        self.name = name
        self.id = id
        self.gpa = gpa

    def __lt__(self, other):
        return self.id < other.id

    def __repr__(self):
        return f"{self.id}"

    def __str__(self):
        return f"{self.name} has id {self.id} with a GPA of {self.gpa}"

x = Student("student x", 900012349, 2.5)
y = Student(42, 900012346, 3.0)
z = Student("freshman", 900012341)

studs = [x, y, z]
print(sorted(studs))

# deque is double ended queue
# queue or stack
from collections import deque
q = deque()
q.append(1)
q.append(2)
q.append(3)
print(q)
q.popleft()
print(q)
s = deque()
s.append(5)
s.append(6)
s.append(7)
print(s)
s.pop()
print(s)

x = [1, 2, 3]
x.pop(0)
print(x)


