from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import time
from spherov2.types import Color
from random import randint

toy = scanner.find_toy(toy_name='SB-0323')
print(toy.name)

with SpheroEduAPI(toy) as droid:
    while True:
        droid.roll(0, 100, 1)
        time.sleep(1)
        droid.roll(180, 100, 1)
        time.sleep(1)