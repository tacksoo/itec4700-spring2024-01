import asyncio
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI, EventType
from spherov2.types import Color
from random import randint

collision_in_progress = False
i = 0

def on_collision(droid: SpheroEduAPI):
    global collision_in_progress
    global i
    if not collision_in_progress:
        collision_in_progress = True
        print(f'Collision detected {i}')
        i += 1
        # Simulate some processing time
        collision_in_progress = False

def main():
    toy = scanner.find_toy(toy_name='SB-FC52')
    with SpheroEduAPI(toy) as droid:
        droid.register_event(EventType.on_collision, on_collision)
        #droid.roll(0, 200, 5)
        while True:
            pass

asyncio.run(main())