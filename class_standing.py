'''
temp = float(input("What is the current temperature? "))

temp += 10

print("It's still cold at: " + str(temp))

# str(), int(), float(), bool()
print(bool("True"))
print(bool(0))
print(bool("AI"))
print(bool([12, 32]))
'''


while True:
    hours = int(input("How many credit hours have you earned? "))
    if not (hours >=0 and hours <= 150):
        print("That is not valid. Number must be between 0 and 150.")
    else:
        if hours < 30:
            print("You are a Freshmen")
        elif hours < 60:
            print("You are a Sophomore")
        elif hours < 90:
            print("You are a Junior")
        else:
            print("You are a Senior")
        break


